import java.util.*

class PartiallyPersistentList<T>() : Iterable<T> {

    private inner class VersionedIterator(val iteratorVersion: Int,
                                          var next: Node? = sortedHeads.floorEntry(iteratorVersion)?.value) : Iterator<T> {
        override fun next(): T {
            if (!hasNext()) throw NoSuchElementException()
            val result = next!!.value
            next = chooseNext(iteratorVersion, next!!)
            return result
        }

        override fun hasNext(): Boolean = next != null

        private fun chooseNext(version: Int, n: Node): Node? {
            val n1 = n.next?.actual()
            val n2 = n1?.duplicate
            return when {
                n1 == null -> null
                n2 == null -> n1
                n1.fromVersion > version -> if (n2.fromVersion > version) null else n2
                else -> n1
            }
        }
    }

    fun appropriateHandle(handle: Handle, version: Int = this.version): Handle {
        val node = handle2Node(handle)
        if (node.fromVersion == version || version == this.version && node.duplicate == null)
            return handle
        if (version == this.version) {
            val result = id2Node[node.id] ?: return Handle(version, false)
            return newHandle(result)
        }
        val iterator = versionIterator(version) as VersionedIterator
        val id = node.id
        while (iterator.hasNext()) {
            if (iterator.next?.id == id)
                return newHandle(iterator.next!!, version)
            iterator.next()
        }
        return Handle(version, false)
    }

    private fun handle2Node(handle: Handle) =
            handle2Node[handle] ?: throw IllegalArgumentException("Node handle is unknown to this list.")

    fun find(value: T, version: Int = this.version): Handle {
        val iterator = versionIterator(version) as VersionedIterator
        while (iterator.hasNext()) {
            val v = iterator.next!!.value
            if (v == value)
                return newHandle(iterator.next!!, version)
            iterator.next()
        }
        return Handle(version, false)
    }

    fun versionIterator(version: Int): Iterator<T> {
        require(version in 0..this.version) { "Version $version is out of versions bounds [0..${this.version}]" }
        return VersionedIterator(version)
    }

    override fun iterator(): Iterator<T> = versionIterator(version)

    private inner class Node(val value: T,
                             val id: Int,
                             var previous: Node?,
                             var next: Node?,
                             var fromVersion: Int,
                             var duplicate: Node?) {
        init {
            id2Node[id] = this
        }

        fun actual() = when {
            duplicate == null -> this
            (duplicate?.fromVersion?.compareTo(fromVersion) ?: 0) > 0 -> duplicate!!
            else -> this
        }
    }

    private val sortedHeads = TreeMap<Int, Node>()
    private var head: Node? = null
    private var tail: Node? = null

    private var lastNodeId: Int = 0
    private fun nextNodeId() = lastNodeId++
    private val id2Node = HashMap<Int, Node>()

    var version: Int = 0; private set
    private val handle2Node = WeakHashMap<Handle, Node>()

    private fun newHandle(node: Node, version: Int = this.version) =
            Handle(version, true).apply { handle2Node[this] = node }

    constructor(from: Iterable<T>) : this() {
        val iterator = from.iterator()
        if (!iterator.hasNext()) {
            return
        } else {
            ++version
            val newNode = Node(iterator.next(), nextNodeId(), null, null, version, null)
            var current = newNode
            while (iterator.hasNext()) {
                val next = Node(iterator.next(), nextNodeId(), current, null, version, null)
                current.next = next
                current = next
            }
            sortedHeads[version] = newNode
            head = newNode
            tail = current
        }
    }

    private tailrec fun updatePrevious(n: Node, newNext: Node?) {
        val newNode = Node(n.value, n.id, n.previous, newNext, version, if (n.duplicate == null) n else null)
        newNext?.previous = newNode

        updateHeadAndTail(n, newNode)

        if (n.duplicate == null)
            n.duplicate = newNode
        else if (n.previous != null)
            updatePrevious(n.previous!!.actual(), newNode)
    }

    private tailrec fun updateNext(n: Node, newPrevious: Node?) {
        val newNode = Node(n.value, n.id, newPrevious, n.next, version, if (n.duplicate == null) n else null)
        newPrevious?.next = newNode

        if (tail == n) {
            tail = newNode
        }

        if (n.duplicate == null)
            n.duplicate = newNode
        else if (n.next != null)
            updateNext(n.next!!.actual(), newNode)
    }

    private fun allowedToChange(handle: Handle) = handle.version == version

    private fun checkAllowedToChange(handle: Handle) {
        check(allowedToChange(handle)) { "Node for this handle is not allowed to be changed anymore." }
    }

    fun add(value: T): Handle {
        ++version

        val newNode = Node(value, nextNodeId(), tail, null, version, null)
        tail = newNode
        if (head == null) {
            head = head ?: newNode
            sortedHeads[newNode.fromVersion] = newNode
        }

        newNode.previous?.let { updatePrevious(it, newNode) }

        return newHandle(newNode)
    }

    fun addAfter(handle: Handle, value: T): Handle {
        checkAllowedToChange(handle)
        val node = handle2Node(handle)
        ++version

        val newNode = Node(value, nextNodeId(), node.actual(), node.next?.actual(), version, null)
        newNode.next?.let { updateNext(it, newNode) }
        newNode.previous?.let { updatePrevious(it, newNode) }
        return newHandle(newNode)
    }

    fun addFirst(value: T): Handle {
        ++version

        val newNode = Node(value, nextNodeId(), null, head, version, null)
        head = newNode
        sortedHeads[newNode.fromVersion] = newNode
        tail = tail ?: newNode
        newNode.next?.let { updateNext(it, newNode) }

        return newHandle(newNode)
    }

    operator fun set(handle: Handle, value: T): Handle {
        checkAllowedToChange(handle)

        val node = handle2Node(handle)
        ++version

        val newNode = Node(value, node.id, node.previous?.actual(), node.next?.actual(), version, if (node.duplicate == null) node else null)

        if (node.duplicate == null) {
            node.duplicate = newNode
            updateHeadAndTail(node, newNode)
        } else {
            node.previous?.let { updatePrevious(it, newNode) }
            node.next?.let { updateNext(it, newNode) }
        }

        return newHandle(newNode)
    }

    private fun updateHeadAndTail(node: Node, newNode: Node) {
        if (head == node) {
            head = newNode
            sortedHeads[newNode.fromVersion] = newNode
        }
        if (tail == node) {
            tail = newNode
        }
    }

    fun remove(handle: Handle): Handle {
        checkAllowedToChange(handle)
        val node = handle2Node(handle)

        ++version

        val fakeNode = Node(node.value, -1, null, null, 0, null)
        node.previous?.let { updatePrevious(it.actual(), fakeNode) }
        val newPrev = fakeNode.previous
        node.next?.let { updateNext(it.actual(), fakeNode) }
        val newNext = fakeNode.next

        newPrev?.next = newNext
        newNext?.previous = newPrev

        id2Node.remove(node.id)

        return if (newNext == null)
            Handle(version, false) else
            newHandle(newNext)
    }

}

fun <T> partiallyPersistentListOf(vararg values: T) = PartiallyPersistentList(values.asIterable())

class Handle(val version: Int, val isPresent: Boolean)

fun <T> PartiallyPersistentList<T>.snapshot(version: Int = this.version) = versionIterator(version).asSequence().toList()